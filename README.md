# RCCodingChallenge
This program was written and run in VSCode, targeting dotnetcore2.0.

It can be run in VSCode by executing the commands dotnet run.
Output of the tests included will be written to a Results.txt file in the working directory.
The program runs from the Coordinator.cs class. 
The implementation of the EventCounter.cs class relies on the HVACUnit.cs and HVACUnitCounter.cs classes.