using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace RC.CodingChallenge
{
    class EventCounter : IEventCounter
    {
        /// <summary>
        /// Parse and accumulate event information from the given log data.
        /// </summary>
        /// <param name="deviceID">ID of the device that the log is associated with (ex: "HV1")</param>
        /// <param name="eventLog">A stream of lines representing time/value recordings.</param>
        public void ParseEvents(string deviceID, StreamReader eventLog) {
            //Create the HVACUnit object and add it to the dictionary which counts occurences of fault sequences
            HVACUnit hvacUnit = new HVACUnit(deviceID);
            HVACUnitCounter.HVACUnits().AddOrUpdate(deviceID, 0, (key, oldValue) => oldValue + 0);
            int lineNumber =0;

            //Read from the eventLog until there's no more.
            while (eventLog.Peek() >= 0)
            {
                string eventLogLine = eventLog.ReadLine();
                lineNumber++;
                string[] logLineSplit = eventLogLine.Split('\t');
                try
                {
                    DateTime lineDateTime = DateTime.ParseExact(logLineSplit[0], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    int lineValue = Int32.Parse(logLineSplit[1]);
                    hvacUnit.UpdateState(lineDateTime, lineValue);
                }
                catch
                {
                    //Something caused the parser/updater to error out on this line. Log the failure and move on to the next line.
                    Debug.WriteLine("Error on line: " + lineNumber);
                }
            }
        }

        /// <summary>
        /// Gets the current count of events detected for the given device
        /// </summary>
        /// <returns>An integer representing the number of detected events</returns>
        public int GetEventCount(string deviceID)
        {
            int errorCount;
            if (HVACUnitCounter.HVACUnits().TryGetValue(deviceID, out errorCount))
            {
                return errorCount;
            }
            return 0;
        }
    }
}
