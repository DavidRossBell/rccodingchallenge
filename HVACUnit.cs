
using System;
namespace RC.CodingChallenge
{
    //An Enum that is used to describe the state of the HVAC Unit. Normal, In the cycling stage of the fail sequence or having suffered a failure.
    public enum HVACState
    {
        Normal,
        CyclingStage,
        Error
    }

    //This class is intended to represent an HVAC unit identified by the deviceID. It contains a state and the last known stage and time of change.
    class HVACUnit {

        public string deviceID;
        public HVACState state;
        //We store the following information for comparison purposes to determine how long the unit has been in a particular stage. 
        //This is used for the detection of the first stage of the fault sequence.
        //We store the last value change instead of the last log line in order to not get thrown off by heartbeats. 
        public DateTime lastValueChangeTime;
        public int lastValue;

        //Constructor. Initialize lastValue to -1, i.e. a number that won't appear in the log. 
        public HVACUnit(string ID)
        {
            this.deviceID = ID;
            this.state = HVACState.Normal;
            this.lastValue = -1;
        }

        //Function responsible for using the current state of the HVACUnit, the time of the log entry and the value of the log entry
        //to determine the state of the HVACUnit after the log entry. i.e. is it in the process of failing, operating normally or has it errored?
        public void UpdateState(DateTime logEntryTime, int value)
        {
            //Based on the current state (Normal or Cycling) we're looking for a specific pattern.
            switch (state)
            {
                case HVACState.Normal:
                    if (lastValue == 3 && value == 2)
                    {
                        //Do the time comparison only if the pattern of value changes match. 
                        TimeSpan difference = logEntryTime - lastValueChangeTime;
                        if (difference.TotalMinutes>=5.0) {
                            state = HVACState.CyclingStage;
                        }
                    }
                    //If the last log entry state was 3 and the difference between the time of the two entries is >5 minutes and the new state is 2, move to the cyclingStage
                    break;
                case HVACState.CyclingStage:
                    //If the new log entry has value 2 or 3 stay in fail state 1, if the value is 0, this is an error, otherwise go to state Normal
                    if (value == 0)
                    {
                        state = HVACState.Error;
                        break;
                    }
                    if (value != 2 && value != 3)
                    {
                        state = HVACState.Normal;
                    }
                    break;
                default:
                    //set the state to normal in the default case.
                    state = HVACState.Normal;
                    break;
            }

            //cleanup, if the value changed then update the time it changed at, if the state is set to error, increment error sequence count and reset to normal
            if (lastValue != value)
            {
                lastValue = value;
                lastValueChangeTime = logEntryTime;
            }

            if (state == HVACState.Error)
            {
                HVACUnitCounter.HVACUnits().AddOrUpdate(deviceID, 1, (key, oldValue) => oldValue + 1);
                state = HVACState.Normal;
            }
        }
    }
}
