using System.Collections.Concurrent;

namespace RC.CodingChallenge
{
    //Static class which holds a concurrent dictionary used to store the number of faults per device.
    //If this were to be developed into a system this class should be replaced by a database, because it's primary purpose is storage.
    public static class HVACUnitCounter
    {
        //Dictionary is private, to force the use of a lazy loader. 
        //I chose a concurrent dictionary in order to protect the dictionary against possible contention. Theoretically that would only happen if you
        //analyzed two log files at the same time belonging to the same device. 
        private static ConcurrentDictionary<string, int> _HVACUnits;

        //Lazy Loader for the concurrent dictionary.
        public static ConcurrentDictionary<string, int> HVACUnits()
        {
            if (_HVACUnits != null)
            {
                return _HVACUnits;
            }
            _HVACUnits = new ConcurrentDictionary<string, int>();
            return _HVACUnits;
        }
    }
}
