using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
namespace RC.CodingChallenge
{
    //This class is used solely to run the program and tests associated with the class specified in the challenge.
    class Coordinator
    {
        static void Main(string[] args)
        {
            //These are some custom test text files. Splitting them up allows for easier detection of which test fails.
            //TestSequences tests the general functionality, i.e. detecting the fault sequence.
            StreamReader reader1 = new StreamReader("TestSequences.txt");
            //TestSequences2 tests the the fault sequence with a heartbeat log line in the middle and other sequences. 
            StreamReader reader2 = new StreamReader("TestSequences2.txt");
            //TestSequences3 tests the the fault sequence with less than 5 minutes in the first 3 stage. We expect there to be no faults here.
            StreamReader reader3= new StreamReader("TestSequences3.txt");

            EventCounter counter = new EventCounter();

            //Create a list of test threads.
            List<Thread> testThreadList = new List<Thread>();
            testThreadList.Add(new Thread(() => counter.ParseEvents("Test1", reader1)));
            testThreadList.Add(new Thread(() => counter.ParseEvents("Test2", reader2)));
            testThreadList.Add(new Thread(() => counter.ParseEvents("Test3", reader3)));

            //Start each thread in the list.
            foreach (Thread testThread in testThreadList)
            {
                testThread.Start();
            }

            //Wait for the threads to complete.
            foreach (Thread testThread in testThreadList)
            {
                testThread.Join();
            }

            //Print a results file.
            if (File.Exists("Results.txt"))
            {
                File.Delete("Results.txt");
            }
            using (StreamWriter writer = new StreamWriter("Results.txt"))
            {
                foreach (KeyValuePair<string, int> HVACUnit in HVACUnitCounter.HVACUnits())
                {
                    writer.WriteLine("Device ID:" + HVACUnit.Key + ", Faults:" + counter.GetEventCount(HVACUnit.Key));
                }
            }
        }
    }
}